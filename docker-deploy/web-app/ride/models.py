from django.db import models
from django.utils import timezone
from django.contrib.postgres.fields import ArrayField



'''
Three choices of vehicle types
'''
CARTYPE_CHOICES = (
    ("S", "S"),
    ("L", "L"),
    ("XL", "XL"),
)



'''
A user with basic info and ride list
'''
class User(models.Model):
    name = models.CharField(max_length = 50)
    email = models.CharField(max_length = 50)
    
    # Whether this user is a driver
    isDriver = models.BooleanField(default = False)

    # Whether this driver is driving (checked only when this user is a driver)
    isDriving = models.BooleanField(default = False)
    
    # The vehicle information
    cartype = models.CharField(max_length = 10, choices = CARTYPE_CHOICES, default = "S")
    plate = models.CharField(default = '', max_length = 10)
    num_seat = models.IntegerField(default = 0)
    special = models.TextField(default = '')
    
    # Print the user name and whether he/she is a driver
    def __str__(self):
        if (self.isDriver):
            return self.name + ' (is a driver)'
        else:
            return self.name + ' (not a driver)'
        


'''
A ride with basic info and user list
'''
class Ride(models.Model):
    # All users of this ride
    user = models.ManyToManyField(User)

    # Owner info
    owner = models.CharField(default = '', max_length = 50)
    num_owner = models.IntegerField(default = 1)

    # Driver info
    driver = models.CharField(max_length = 50, blank = True)

    # Sharer info
    sharer = ArrayField(models.CharField(max_length = 50, null = True, blank = True), null = True, blank = True)
    num_sharer = ArrayField(models.IntegerField(default = 0), null = True, blank = True)
    
    # The vehicle information
    cartype = models.CharField(max_length = 10, choices = CARTYPE_CHOICES, default = "S")
    plate = models.CharField(default = '', max_length = 10)
    special = models.TextField(default = '')
    
    # True -> shared ride; False -> private ride
    isShare = models.BooleanField(default = False)

    # True -> open; Flase -> confirmed
    isOpen = models.BooleanField(default = True)

    # True -> completed; False: not completed
    isComplete = models.BooleanField(default = False)

    # The destination of this ride
    destination = models.CharField(max_length = 50)

    # The arrival time of this ride
    arrival = models.DateTimeField('arrival time')

    # The number of passengers
    num_passenger = models.IntegerField(default = 0)

    # Print the ride info
    def __str__(self):
        return 'A ride to ' + self.destination


