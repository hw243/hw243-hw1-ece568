import datetime

from django.test import TestCase
from django.utils import timezone

from .models import Ride, User

class RideModelTests(TestCase):
    def test_request_ride_with_3_users(self):
        u1 = User(name = 'wqy')
        u1.save()
        r1 = u1.ride_set.create(destination = 'Durham', arrival = timezone.now() + datetime.timedelta(hours = 1), num_passenger = 1, isShare = True)
        self.assertIs(r1.destination, 'Durham')
