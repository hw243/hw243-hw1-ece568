from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name = 'home'),
    path('request-ride/', views.request_ride, name = 'request-ride'),
    path('share-search/', views.share_search, name = 'share-search'),
    path('share-search/<int:ride_id>/<int:num_sharer>/', views.share_detail, name = 'share-detail'),
    path('share-search/<int:ride_id>/<int:num_sharer>/share-join/', views.share_join, name = 'share-join'),
    path('my-rides/', views.my_rides, name = 'my-rides'),
    path('my-rides/<int:ride_id>/', views.my_rides_detail, name = 'my-rides-detail'),
    path('my-rides/<int:ride_id>/owner-delete/', views.owner_delete, name = 'owner-delete'),
    path('my-rides/<int:ride_id>/my-rides-edit/', views.my_rides_edit, name = 'my-rides-edit'),
    path('my-rides/<int:ride_id>/share-quit/', views.share_quit, name = 'share-quit'),
    path('user-info/', views.user_info, name = 'user-info'),
    path('user-edit/', views.user_edit, name = 'user-edit'),
    path('go-drive/', views.go_drive, name = 'go-drive'),
    path('go-drive/<int:ride_id>/', views.go_drive_open_detail, name = 'go-drive-open-detail'),
    path('go-drive/<int:ride_id>/confirm/', views.go_drive_open_confirm, name = 'go-drive-open-confirm'),
    path('go-drive/complete/', views.go_drive_complete, name = 'go-drive-complete'),
    path('driver-registration/', views.driver_registration, name = 'driver-registration'),
    path('driver-info/', views.driver_info, name = 'driver-info'),
    path('driver-edit/', views.driver_edit, name = 'driver-edit'),
]

