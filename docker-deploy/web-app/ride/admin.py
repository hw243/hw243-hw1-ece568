from django.contrib import admin

from .models import User, Ride

class UserAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name']}),
        ('Driver or not', {'fields': ['isDriver']}),
    ]

class RideAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['destination']}),
        ('Arrival time', {'fields': ['arrival']}),
        ('Shared or not', {'fields': ['isShare']}),
        ('Open or not', {'fields': ['isOpen']}),
        ('Complete or not', {'fields': ['isComplete']}),
    ]
    
admin.site.register(User, UserAdmin)
admin.site.register(Ride, RideAdmin)
