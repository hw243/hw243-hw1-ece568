# Generated by Django 3.0.2 on 2020-01-22 20:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ride', '0002_auto_20200121_1606'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Car',
        ),
        migrations.RenameField(
            model_name='user',
            old_name='user_name',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='ride',
            name='driver',
        ),
        migrations.RemoveField(
            model_name='ride',
            name='sharer',
        ),
        migrations.AddField(
            model_name='ride',
            name='cartype',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='ride',
            name='plate',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='user',
            name='cartype',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='user',
            name='num_seat',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='user',
            name='plate',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='user',
            name='special',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='ride',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ride.User'),
        ),
    ]
