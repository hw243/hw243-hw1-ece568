# Generated by Django 3.0.2 on 2020-01-22 21:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ride', '0005_auto_20200122_2131'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ride',
            name='driver',
        ),
        migrations.AddField(
            model_name='ride',
            name='driver',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='driver', to='ride.User'),
        ),
        migrations.RemoveField(
            model_name='ride',
            name='owner',
        ),
        migrations.AddField(
            model_name='ride',
            name='owner',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='owner', to='ride.User'),
        ),
        migrations.AlterField(
            model_name='ride',
            name='sharer',
            field=models.ManyToManyField(to='ride.User'),
        ),
    ]
