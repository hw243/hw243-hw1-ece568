# Generated by Django 3.0.2 on 2020-01-28 03:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ride', '0009_user_isdriving'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ride',
            old_name='num_passenger',
            new_name='num_remaining_seat',
        ),
    ]
