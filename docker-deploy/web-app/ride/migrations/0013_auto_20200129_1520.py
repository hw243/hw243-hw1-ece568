# Generated by Django 3.0.2 on 2020-01-29 15:20

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ride', '0012_auto_20200128_0510'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='email',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ride',
            name='cartype',
            field=models.CharField(choices=[('1', 'S'), ('2', 'L'), ('3', 'XL')], default='1', max_length=10),
        ),
        migrations.AlterField(
            model_name='ride',
            name='destination',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='ride',
            name='driver',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='ride',
            name='owner',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='ride',
            name='plate',
            field=models.CharField(default='', max_length=10),
        ),
        migrations.AlterField(
            model_name='ride',
            name='sharer',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, max_length=50, null=True), blank=True, null=True, size=None),
        ),
        migrations.AlterField(
            model_name='user',
            name='cartype',
            field=models.CharField(choices=[('1', 'S'), ('2', 'L'), ('3', 'XL')], default='1', max_length=10),
        ),
        migrations.AlterField(
            model_name='user',
            name='name',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='user',
            name='plate',
            field=models.CharField(default='', max_length=10),
        ),
    ]
