from django import forms
from django.contrib.auth.models import User as LogUser
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm, Form
from ride.models import User, Ride
from django.utils import timezone



'''
Three choices of vehicle types
'''
CARTYPE_CHOICES = (
    ("", ""),
    ("S", "S"),
    ("L", "L"),
    ("XL", "XL"),
)



'''
A model form for ride creation
'''
class RideForm(ModelForm):
    cartype = forms.ChoiceField(choices = CARTYPE_CHOICES, required = False)
    special = forms.CharField(widget = forms.Textarea, required = False)
    
    class Meta:
        model = Ride
        fields = ['destination', 'arrival', 'num_owner', 'isShare']
        labels = {
            'arrival': "Arriving time",
            'num_owner': "Number of passengers",
            'isShare': "Share this ride with others?"
        }

    def clean_arrival(self):
        arrival = self.cleaned_data.get('arrival')
        if arrival < timezone.now():
            raise forms.ValidationError("The arriving time should be in the future.")
        return arrival



'''
A form for user registration
'''
class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = LogUser
        fields = ['username', 'email', 'password1', 'password2']



'''
A form for driver registration
'''
class DriverRegisterForm(Form):
    cartype = forms.ChoiceField(label = "Vehicle type", choices = CARTYPE_CHOICES)
    plate = forms.CharField(label = "Plate number", max_length = 10)
    num_seat = forms.IntegerField(label = "Number of total seats")
    special = forms.CharField(widget = forms.Textarea, label = "Special vehicle info", required = False)



'''
A form for sharer to search for suitable rides
'''
class ShareForm(Form):
    destination = forms.CharField(max_length = 50)
    arrival_early = forms.DateTimeField(label = "The earliest arriving time")
    arrival_late = forms.DateTimeField(label = "The latest arriving time")
    num_passenger = forms.IntegerField(label = "Number of passengers")

    def clean_arrival_early(self):
        arrival_early = self.cleaned_data.get('arrival_early')
        if arrival_early < timezone.now():
            raise forms.ValidationError("Invalid time")
        return arrival_early

    def clean_arrival_late(self):
        arrival_late = self.cleaned_data.get('arrival_late')
        if arrival_late < timezone.now():
            raise forms.ValidationError("Invalid time")
        return arrival_late

    

'''
A form for the ride owner to edit request
'''
class RideEditForm(Form):
    destination = forms.CharField(max_length = 50)
    arrival = forms.DateTimeField(label = "Arriving time")
    num_owner = forms.IntegerField(label = "Number of passengers")
    isShare = forms.BooleanField(label = "Share this ride with others?", required = False)
    cartype = forms.ChoiceField(label = "Vehicle type", choices = CARTYPE_CHOICES)
    special = forms.CharField(widget = forms.Textarea, label = "Special request", required = False)



'''
A form for user to edit email
'''
class UserEditForm(Form):
    email = forms.CharField(max_length = 50)
