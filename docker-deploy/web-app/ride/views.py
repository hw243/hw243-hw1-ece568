import datetime

from django.shortcuts import render, redirect
from ride.models import User, Ride
from .forms import UserRegisterForm, DriverRegisterForm, RideForm, ShareForm, RideEditForm, UserEditForm
from django.db.models import Q
from django.core.mail import send_mail



'''
User registration page
'''
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            user_name = form.cleaned_data.get('username')
            user_email = form.cleaned_data.get('email')
            User.objects.create(name = user_name, email = user_email)
            return redirect('/login')
    else:
        form = UserRegisterForm()
    return render(request, 'ride/register.html', {'form': form})



'''
Home page
Provide links to sign in and sign up
Provide links to ride-related pages after login
'''
def home(request):
    if not request.user.is_authenticated:
        return render(request, 'ride/welcome.html')
    else:
        return render(request, 'ride/home.html', {'user_name': request.user.username})



'''
Request a ride as owner
'''
def request_ride(request):
    if request.method == 'POST':
        form = RideForm(request.POST)
        if form.is_valid():
            inst = form.save()
            inst.owner = request.user.username
            inst.num_passenger = form.cleaned_data.get('num_owner')
            inst.cartype = form.cleaned_data.get('cartype')
            inst.special = form.cleaned_data.get('special')
            inst.user.add(User.objects.get(name = request.user.username))
            inst.save()
            return render(request, 'ride/request_success.html')
    else:
        form = RideForm
        
    context = {
        'user_name': request.user.username,
        'form': form,
    }
    return render(request, 'ride/request_ride.html', context)



'''
Type in details to search for a ride as sharer
'''
def share_search(request):
    if request.method == 'POST':
        form = ShareForm(request.POST)
        if form.is_valid():
            sharer_destination = form.cleaned_data.get('destination')
            sharer_arrival_early = form.cleaned_data.get('arrival_early')
            sharer_arrival_late = form.cleaned_data.get('arrival_late')
            sharer_num_passenger = form.cleaned_data.get('num_passenger')
            ride_list = Ride.objects.filter(
                destination = sharer_destination,
                arrival__gte = sharer_arrival_early,
                arrival__lte = sharer_arrival_late,
                isOpen = True,
                isComplete = False).exclude(
                    owner = request.user.username).exclude(
                        sharer__contains = [request.user.username])
            context = {
                'suitable_rides': ride_list,
                'num_sharer': sharer_num_passenger
            }
            return render(request, 'ride/share_result.html', context)
    else:
        form = ShareForm
    return render(request, 'ride/share_search.html', {'form': form})



'''
View the detail of a suitable ride
'''
def share_detail(request, ride_id, num_sharer):
    context = {
        'ride': Ride.objects.get(id = ride_id),
        'num_sharer': num_sharer
    }
    return render(request, 'ride/share_detail.html', context)



'''
Join a suitable ride
'''
def share_join(request, ride_id, num_sharer):
    curr_user = User.objects.get(name = request.user.username)
    curr_ride = Ride.objects.get(id = ride_id)
    if curr_ride.sharer == None:
        curr_ride.sharer = [request.user.username]
    else:
        curr_ride.sharer.append(request.user.username)
    if curr_ride.num_sharer == None:
        curr_ride.num_sharer = [num_sharer]
    else:
        curr_ride.num_sharer.append(num_sharer)
    curr_ride.num_passenger = curr_ride.num_passenger + num_sharer
    curr_ride.user.add(curr_user)
    curr_user.save()
    curr_ride.save()
    return render(request, 'ride/share_join.html')



'''
View all rides the user attend as owner/sharer
'''
def my_rides(request):
    context = {
        'my_rides_list': User.objects.get(name = request.user.username).ride_set.filter(isComplete = False).order_by('arrival')
    }
    return render(request, 'ride/my_rides.html', context)



'''
View the detail of an attended ride
'''
def my_rides_detail(request, ride_id):
    context = {
        'user_name': request.user.username,
        'ride': Ride.objects.get(id = ride_id)
    }
    return render(request, 'ride/my_rides_detail.html', context)



'''
Edit the ride request
'''
def my_rides_edit(request, ride_id):
    if request.method == 'POST':
        form = RideEditForm(request.POST)
        if form.is_valid():
            curr_user = User.objects.get(name = request.user.username)
            curr_ride = Ride.objects.get(id = ride_id)
            edit_destination = form.cleaned_data.get('destination')
            edit_arrival = form.cleaned_data.get('arrival')
            edit_num_owner = form.cleaned_data.get('num_owner')
            edit_isShare = form.cleaned_data.get('isShare')
            edit_cartype = form.cleaned_data.get('cartype')
            edit_special = form.cleaned_data.get('special')

            curr_ride.destination = edit_destination
            curr_ride.arrival = edit_arrival
            curr_ride.num_passenger = curr_ride.num_passenger - curr_ride.num_owner + edit_num_owner
            curr_ride.num_owner = edit_num_owner
            curr_ride.isShare = edit_isShare    
            curr_ride.cartype = edit_cartype
            curr_ride.special = edit_special
            curr_ride.save()
            return render(request, 'ride/my_rides_edit_success.html')
    else:
        form = RideEditForm
    return render(request, 'ride/my_rides_edit.html', {'form': form})    



'''
A view for owner to delete the ride
'''
def owner_delete(request, ride_id):
    curr_ride = Ride.objects.get(id = ride_id)
    curr_ride.delete()
    return render(request, 'ride/owner_delete.html')



'''
A vew of user information
'''
def user_info(request):
    context = {
        'user': User.objects.get(name = request.user.username)
    }
    return render(request, 'ride/user_info.html', context)



'''
A view for user to edit email
'''
def user_edit(request):
    if request.method == 'POST':
        form = UserEditForm(request.POST)
        if form.is_valid():
            curr_user = User.objects.get(name = request.user.username)
            curr_user.email = form.cleaned_data.get('email')
            curr_user.save()
            return render(request, 'ride/user_edit_success.html')
    else:
        form = UserEditForm
    return render(request, 'ride/user_edit.html', {'form': form})



'''
A view for a sharer to quit the ride
'''
def share_quit(request, ride_id):
    curr_user = User.objects.get(name = request.user.username)
    curr_ride = Ride.objects.get(id = ride_id)
    curr_ride.user.remove(curr_user)
    index = curr_ride.sharer.index(curr_user.name)
    del curr_ride.sharer[index]
    num_sharer = curr_ride.num_sharer[index]
    curr_ride.num_passenger = curr_ride.num_passenger - num_sharer
    del curr_ride.num_sharer[index]
    curr_user.save()
    curr_ride.save()
    return render(request, 'ride/share_quit.html')



'''
A page for users who have registered as drivers
If not a driver: Provide a link to "driver-registration"
Drivers:
  If he/she has a confirmed but not completed ride, show its info and provide a "complete" button
  If he/she has not started a ride yet, show all the rides that match the request info
'''
def go_drive(request):
    curr_user = User.objects.get(name = request.user.username)
    if not curr_user.isDriver:
        return render(request, 'ride/not_driver.html')
    else:
        if curr_user.isDriving:
            context = {
                'ride': Ride.objects.get(driver = request.user.username, isOpen = False, isComplete = False)
            }
            return render(request, 'ride/go_drive_confirm.html', context)
        else:
            context = {
                'curr_rides_list': Ride.objects.filter(Q(isOpen = True)).filter(Q(num_passenger__lte = curr_user.num_seat)).filter(Q(cartype = '') | Q(cartype = curr_user.cartype)).filter(Q(special = '') | Q(special = curr_user.special)).exclude(owner = request.user.username).exclude(sharer__contains = [request.user.username]).order_by('arrival')
            }
            return render(request, 'ride/go_drive_open.html', context)



'''
The view for driver to check ride detail and decide whether he/she confirms it
'''
def go_drive_open_detail(request, ride_id):
    context = {
        'ride': Ride.objects.get(id = ride_id)
    }
    return render(request, 'ride/go_drive_open_detail.html', context)



'''
The view that shows successful confirmation
'''
def go_drive_open_confirm(request, ride_id):
    # Make this ride confirmed
    curr_user = User.objects.get(name = request.user.username)
    curr_ride = Ride.objects.get(id = ride_id)
    curr_user.isDriving = True
    curr_ride.driver = request.user.username
    curr_ride.cartype = curr_user.cartype
    curr_ride.plate = curr_user.plate
    curr_ride.special = curr_user.special
    curr_ride.isOpen = False
    curr_user.save()
    curr_ride.save()

    # Send emails
    subject = "An important message from RideSharing App!"
    message = "Hi! Your ride to %s is just confirmed!" % curr_ride.destination
    curr_owner = User.objects.get(name = curr_ride.owner)
    send_mail(subject, message, 'ridesharingWW@outlook.com', [curr_owner.email])
    if curr_ride.sharer:
        for sh in curr_ride.sharer:
            curr_sharer = User.objects.get(name = sh)
            send_mail(subject, message, 'ridesharingWW@outlook.com', [curr_sharer.email])

    return render(request, 'ride/go_drive_open_confirm.html')



'''
The view that shows successful completion
'''
def go_drive_complete(request):
    curr_user = User.objects.get(name = request.user.username)
    curr_ride = Ride.objects.get(driver = request.user.username, isOpen = False, isComplete = False)
    curr_user.isDriving = False
    curr_ride.isComplete = True
    curr_user.save()
    curr_ride.save()
    return render(request, 'ride/go_drive_complete.html')



'''
A view for driver registration
If the user has registered, provide a link to "go drive"
'''
def driver_registration(request):
    if request.method == 'POST':
        form = DriverRegisterForm(request.POST)
        if form.is_valid():
            curr_user = User.objects.get(name = request.user.username)
            curr_user.isDriver = True
            curr_user.cartype = form.cleaned_data.get('cartype')
            curr_user.plate = form.cleaned_data.get('plate')
            curr_user.num_seat = form.cleaned_data.get('num_seat')
            curr_user.special = form.cleaned_data.get('special')
            curr_user.save()
            return render(request, 'ride/driver_register_success.html')
    else:
        form = DriverRegisterForm
    
    context = {
        'user_name': request.user.username,
        'isDriver': User.objects.get(name = request.user.username).isDriver,
        'form': form,
    }
    return render(request, 'ride/driver_register.html', context)



'''
A view of this driver's info
'''
def driver_info(request):
    curr_user = User.objects.get(name = request.user.username)
    if not curr_user.isDriver:
        return render(request, 'ride/not_driver.html')
    else:
        return render(request, 'ride/driver_info.html', {'user': curr_user})




'''
A view for driver to edit his/her vehicle info
'''
def driver_edit(request):
    curr_user = User.objects.get(name = request.user.username)
    if request.method == 'POST':
        form = DriverRegisterForm(request.POST)
        if form.is_valid():
            curr_user.cartype = form.cleaned_data.get('cartype')
            curr_user.plate = form.cleaned_data.get('plate')
            curr_user.num_seat = form.cleaned_data.get('num_seat')
            curr_user.special = form.cleaned_data.get('special')
            curr_user.save()
            return render(request, 'ride/driver_edit_success.html')
    else:
        form = DriverRegisterForm
    return render(request, 'ride/driver_edit.html', {'form': form})
