# hw243-hw1-ece568
&emsp;Group members:<br>
&emsp;&emsp;Hancong Wang (hw243)<br>
&emsp;&emsp;Qingyu Wu (qw90)<br>

## Ride Sharing Web-App

### 1. User Registration:
&emsp;A user can register by providing his/her username, email address and password.<br>

### 2. Login:
&emsp;A user can login with a valid account.<br>

### 3. Home page:
&emsp;If the user has not logged in, just show the welcome page.<br>
&emsp;If logged in, this home page has 8 options:<br>
&emsp;&emsp;(1) Request a ride:<br>
&emsp;&emsp;&emsp;Users can input some ride info to request a ride as owner.<br>
&emsp;&emsp;(2) Join a ride:<br>
&emsp;&emsp;&emsp;Users can input some ride info to search for a ride to join as sharer.<br>
&emsp;&emsp;(3) My rides:<br>
&emsp;&emsp;&emsp;Users can check all the rides they attend as owner/sharer.<br>
&emsp;&emsp;(4) User info:<br>
&emsp;&emsp;&emsp;Users can check their individual information.<br>
&emsp;&emsp;(5) Go drive:<br>
&emsp;&emsp;&emsp;A registered driver can view, start (confirm) and complete rides.<br>
&emsp;&emsp;(6) Driver registration:<br>
&emsp;&emsp;&emsp;A user can register to become a valid driver.<br>
&emsp;&emsp;(7) Vehicle info:<br>
&emsp;&emsp;&emsp;A registered driver can check the vehicle information.<br>
&emsp;&emsp;(8) Logout:<br>
&emsp;&emsp;&emsp;A user can logout.<br>
&emsp;(p.s. Nearly every page has a button to return to this home page.)<br>

### 4. Request a ride:
&emsp;The user needs to provide a destination, an arriving time, the number of passengers in his/her party to request a ride as owner.<br>

### 5. Join a ride:
&emsp;The user can provide a destination, an arriving time window, and the number of passengers in his/her party to search for a valid ride.<br>
&emsp;If there are valid rides, he/she can choose to view their detail.<br>
&emsp;In the detail view, he/she can choose to join it.<br>

### 6. My rides:
&emsp;The user can view all the rides he/she attends as owner/sharer.<br>
&emsp;The ride list shows the destination, the arriving time, and the status (open/confirmed).<br>
&emsp;After clicking on the ride, the user can view all the detail of this ride.<br>
&emsp;If the user is the owner of this ride, he/she can edit/delete the request information.<br>
&emsp;&emsp;If the user change "isShare" from True to False, the existing sharers will not be kicked out. However, no one can join any more.<br>
&emsp;If the user is one of the sharers of this ride, he/she can quit this ride.<br>

### 7. User info:
&emsp;The user can view his/her individual information.<br>
&emsp;The user can change his/her email.<br>

### 8. Go drive:
&emsp;If the user is not a driver, a link to the "driver registration" page is provided.<br>
&emsp;If the user is a registered driver:<br>
&emsp;&emsp;If the user is not currently driving for a ride:<br>
&emsp;&emsp;&emsp;Show all rides that match the user's vehicle information.<br>
&emsp;&emsp;&emsp;The user can click on a ride to view the detail of this ride.<br>
&emsp;&emsp;&emsp;In the detail page, the user can confirm and start this ride.<br>
&emsp;&emsp;If the user is currently driving for a ride:<br>
&emsp;&emsp;&emsp;Show the detail of this ride.<br>
&emsp;&emsp;&emsp;The user can choose to complete this ride.<br>

### 9. Driver registration:
&emsp;If the user is a registered driver, a link to the "go drive" page is provided.<br>
&emsp;If the user has not registered yet, he/she can provide his/her vehicle information to register as a driver.<br>

### 10. Vehicle info:
&emsp;If the user is not a driver, a link to the "driver registration" page is provided.<br>
&emsp;If the user is a registered driver, his/her vehicle information is displayed.<br>
&emsp;&emsp;The user can edit his/her vehicle information.<br>

### 11. Logout:
&emsp;The user can logout and return to the welcome page.<br>